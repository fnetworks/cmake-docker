ARG compiler
FROM $compiler:latest

ARG cmake-version=3.15.2

RUN wget https://github.com/Kitware/CMake/releases/download/v${cmake-version}/cmake-${cmake-version}-Linux-x86_64.sh \
      -q -O /tmp/cmake-install.sh \
      && chmod u+x /tmp/cmake-install.sh \
      && /tmp/cmake-install.sh --skip-license \
      && rm /tmp/cmake-install.sh